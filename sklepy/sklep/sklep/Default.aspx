﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<script runat="server">        
    void SortButton_Click(Object sender, EventArgs e)
    { 
        String expression = SortList1.SelectedValue;
        SortDirection direction2 = SortDirection.Ascending;
        if (DirectionList1.SelectedValue == "DESC")
            direction2 = SortDirection.Descending;
        ListView1.Sort(expression, direction2);

    }


</script>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <table style="margin:20px">
            <tr>
                <td><asp:TextBox ID="serachtb" runat="server" /></td>
                <td><asp:Button ID="serachb" runat="server" OnClick="serachb_Click" Text="Search"/></td>
                
            </tr>
            <tr >
                <td colspan="2"><p></p></td>
            </tr>
        <tr>
          <td>Sort by:</td>
          <td>
            <asp:DropDownList ID="SortList1" runat="server">
              <asp:ListItem>name</asp:ListItem>
              <asp:ListItem Selected="true">price</asp:ListItem>
              <asp:ListItem>producent</asp:ListItem>       
            </asp:DropDownList>
          </td>
          <td>Sort order:</td>
          <td>
            <asp:DropDownList ID="DirectionList1" runat="server">
              <asp:ListItem Value="ASC" Text="Ascending" Selected="True" />
              <asp:ListItem Value="DESC" Text="Descending" />
            </asp:DropDownList>
          </td>
        </tr>
            <tr>
                <td><asp:Button id="SortButton"
        Text="Sort"
        OnClick="SortButton_Click" 
        runat="server"/>  </td>
            </tr>
      </table>

      

    <asp:ListView  ID="ListView1" runat="server" DataSourceID="SqlDataSource1" EditIndex="1">
        <LayoutTemplate>
        <div id="itemPlaceholder" style="float:left" runat="server"></div>
    </LayoutTemplate>
        <ItemTemplate>
            <div style="float:left; margin:5px;width:300px;height:250px;">
                <table style="background-color:rgba(147, 147, 147, 0.1)">
                    <tr>
                        <td><img style="width: 140px;height: 120px;" src="<%# Eval("img")%>" /></td>
                        <td>
                            <a runat="server" href=<%# "~/Basket?add="+Eval("id")%>><img style="width: 48px;height: 48px;" src="http://freeflaticons.com/wp-content/uploads/2014/09/shopping-cart-free-commerce-icons-1410266092ngk48.png" /></a> 
                        </td>
                    </tr>
                    <tr>
                        <td>Nazwa: <%# Eval("name")%></td>
                    </tr>
                    <tr>
                        <td>Producent: <%# Eval("producent")%></td>
                    </tr>
                    <tr>
                        <td>Cena: <%# Eval("price")%> zł</td>
                    </tr>
                    <tr>
                        <td>
                            <a runat="server" href=<%# "~/Product?id="+Eval("id")%>>Przejdz do strony produktu</a>
                        </td>
                        
                    </tr>
                    
                </table>
            </div>
        </ItemTemplate>
    </asp:ListView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sklepConnectionString %>" ProviderName="<%$ ConnectionStrings:sklepConnectionString.ProviderName %>" SelectCommand="SELECT * FROM products"></asp:SqlDataSource>
    </div>










</asp:Content>
