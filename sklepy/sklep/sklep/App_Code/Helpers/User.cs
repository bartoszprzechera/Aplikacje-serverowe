﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using sklep.Helpers;

/// <summary>
/// Data klasa dla uzytkownika strony
/// </summary>
namespace sklep.Helpers
{
    public class User:MYSQLItem
    {
        public string ID { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string PostCode { get; set; }
        public List<Order> userOrders { get; set; }
        public List<Rating> userRatings { get; set; }
        private SQLConnector con;
        public User()
        {
            con = new SQLConnector();
        }
        public override void insert()
        {
            this.ID = con.insert("INSERT INTO users (username, password, email, activated, city, street, postcode) VALUES ('" + this.Login + "', '" + this.Password + "', '" + this.Email + "', '"+0+ "', '" + this.City + "', '" + this.Street + "', '" + this.PostCode + "') ").ToString();

        }
        public override void update() {
            con.updateOrDelete("UPDATE users SET username='" + this.Login + "', password='" + this.Password + "', city='" + this.City + "', street='" + this.Street + "', postcode='" + this.PostCode + "', email='" + this.Email + "' WHERE id='" + this.ID + "'");
        }
        public override void delete() {
            con.updateOrDelete("DELETE FROM users WHERE id='" + this.ID + "'");
        }
        public override void updateStatus() {
            MySqlDataReader reader = con.reader("SELECT * FROM users WHERE id='" + this.ID + "'");
            while (reader.Read())
            {
                this.Login = (string)reader["username"];
                this.Password = (string)reader["password"];
                this.Email = (string)reader["email"];
                this.City = (string)reader["city"];
                this.Street = (string)reader["street"];
                this.PostCode = (string)reader["postcode"];
            }
        }
        public void getOrders() {
            MySqlDataReader reader = con.reader("SELECT * FROM orders WHERE user='" + this.ID + "'");
            this.userOrders = new List<Order>();
            while (reader.Read())
            {
                Order temp = new Order();
                temp.user = this;
                temp.ID = (string)reader["id"].ToString();
                temp.updateStatus();
                this.userOrders.Add(temp);
            }
        }
        public void getRatings() {
            MySqlDataReader reader = con.reader("SELECT * FROM ratings WHERE user='" + this.ID + "'");
            this.userRatings = new List<Rating>();
            while (reader.Read())
            {
                Rating temp = new Rating();
                temp.userlogin = this.Login;
                temp.ID = (string)reader["id"].ToString();
                temp.updateStatus();
                this.userRatings.Add(temp);
            }
        }
        public static User login(string Login,string Password) {
            SQLConnector conn=new SQLConnector();
            User temp = new User();
            MySqlDataReader reader = conn.reader("SELECT * FROM users WHERE username='" + Login + "' AND password='"+ Password + "'");
            while (reader.Read())
            {
                temp.ID = reader["id"].ToString();
                temp.updateStatus();
                return temp;
            }
            return null;
        }
        public static User getUserById(string Id)
        {
            User resoult = new User();
            resoult.ID = Id;
            resoult.updateStatus();
            return resoult;
        }
    }
}