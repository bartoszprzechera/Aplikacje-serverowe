﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using sklep;
using sklep.Helpers;

public partial class Account_Manage : System.Web.UI.Page
{
   
    

    protected void Page_Load()
    {
        if (Session["userid"] != null)
        {
            SqlDataSource1.SelectCommand = "SELECT * FROM orders WHERE user='" + Session["userid"] + "'";
            orders.DataBind();
        }
        else {
            Response.Redirect("~/");

        }
    }

    protected void ChangePassword_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            User temp = new User();
            temp.ID = Session["userid"].ToString();
            temp.updateStatus();
            if (temp.Password == CurrentPassword.Text) {
                temp.Password = NewPassword.Text;
                temp.update();
            }
            
        }
    }

    protected void updateadres_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            User temp = new User();
            temp.ID = Session["userid"].ToString();
            temp.updateStatus();
            temp.City = miasto.Text;
            temp.Street = ulica.Text;
            temp.PostCode = postcode.Text;
            temp.update();
        }
    }

   
}