﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <br />
    <br />
    <div class="row">

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:projektConnectionString %>" ProviderName="<%$ ConnectionStrings:projektConnectionString.ProviderName %>" SelectCommand="SELECT * FROM produkty"></asp:SqlDataSource>


    <asp:DataList ID="DataList1" runat="server" DataSourceID="SqlDataSource1"  Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" RepeatColumns="3" RepeatDirection="Horizontal" ShowFooter="False" ShowHeader="False" HorizontalAlign="Center" >
        
        <AlternatingItemStyle BackColor="#666666" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" ForeColor="White" HorizontalAlign="Justify" />
        <ItemStyle  Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Justify" />
         
        <ItemTemplate>
            <div style="float:left;width:450px;height:400px">
                <table>
                    <tr >
                        <td rowspan="4" ><img style="width:100px;height:100px" src="<%# Eval("zdjecie")%>" /></td>
                    
                        <td>Nazwa: <%# Eval("nazwa")%></td>
                    </tr>
                    <tr>
                        <td>Producent: <%# Eval("producent")%></td>
                    </tr>
                    <tr>
                        <td>Cena: <%# Eval("cena")%></td>
                        <td>
                            <a runat="server" href=<%# "~/Product?produktid="+Eval("id")%>>Dalsze szczegóły</a>
                        </td>
                    </tr>
                    <tr>
                         <td>
                            <a runat="server" href="/"><img style="width:100px" src="https://image.flaticon.com/icons/png/512/2/2054.png" /></a> 
                        </td>
                    </tr>
                </table>
            </div>
        </ItemTemplate>
        
    </asp:DataList>







        </div>
</asp:Content>
