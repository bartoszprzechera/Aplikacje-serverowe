﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Web.UI;
using sklep;
using sql;
using MySql.Data.MySqlClient;

public partial class Account_Login : Page
{
    private SQL con = new SQL();
    protected void Page_Load(object sender, EventArgs e)
    {
        errorlabel.Text = "";
    }

    protected void LogIn(object sender, EventArgs e)
    {
        if (IsValid)
        {
            MySqlDataReader reader = con.reader("SELECT * FROM klienci WHERE login='" + UserName.Text + "' AND haslo='" + Password.Text + "'");
            if (reader.Read())
            {
                Session["ktojestzalogowany_id"] = reader["id"].ToString();
                Session["ktojestzalogowany_login"] = reader["login"].ToString();
                Response.Redirect("~/Account/Manage");
            }
            else
            {
                errorlabel.Text = "Popraw login lub/i hasło :D";
            }
        }
    }
}