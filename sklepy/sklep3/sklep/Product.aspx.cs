﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using sklep;
using sql;

public partial class Product : Page
{
    private SQL con = new SQL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["produktid"] == null)
        {
            Response.Redirect("~/");
        }
        MySqlDataReader reader = con.reader("SELECT * FROM produkty WHERE id='"+ Request.QueryString["produktid"] + "'");
        if (reader.Read())
        {
            Cena.Text ="Cena: "+ reader["cena"].ToString()+"zł";
            Producent.Text ="Producent:"+ reader["producent"].ToString();
            ProductImage.ImageUrl =reader["zdjecie"].ToString();
            Nazwa.Text = "Nazwa:" + reader["nazwa"].ToString();
            Opis.Text = "Opis produktu:" + reader["opis"].ToString();
        }
        else {
            Response.Redirect("~/");
        }
    }


    protected void buttonKup_Click(object sender, ImageClickEventArgs e)
    {
        //dodanei do koszyka
    }
}