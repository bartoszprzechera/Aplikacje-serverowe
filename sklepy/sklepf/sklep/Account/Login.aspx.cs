﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Web.UI;
using sklep;

public partial class Account_Login : Page
{
        protected void Page_Load(object sender, EventArgs e)
        {
         
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid){
            Uzytkownik temp =sklep.Uzytkownik.loguj(UserName.Text, Password.Text);
            if (temp != null)
            {
                Session["uzytkowniknumber"] = temp.ID;
                Session["uzytkowniknazwa"] = UserName.Text;
                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            else {
                errorlabel.Text = "Login lub Hasło są niepoprawne.";
            }

            }
        }
}