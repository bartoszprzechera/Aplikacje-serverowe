﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class administracja_users : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["uzytkowniknumber"] == null && Session["uzytkowniknazwa"] != null && Session["uzytkowniknazwa"].ToString() == "admin")
        {
            Response.Redirect("~/");
        }
    }
    protected void lbInsert_Click(object sender, EventArgs e)
    {
        try
        {
            UsersSqlDataSource.Insert();
        }
        catch (Exception exe) { }
    }
    protected void insertusersql(object sender, SqlDataSourceCommandEventArgs e)
    {
        e.Command.Parameters["username"].Value = ((TextBox)gvUsers.FooterRow.FindControl("tbusername")).Text;
        e.Command.Parameters["email"].Value = ((TextBox)gvUsers.FooterRow.FindControl("tbemail")).Text;
        e.Command.Parameters["password"].Value = ((TextBox)gvUsers.FooterRow.FindControl("tbpassword")).Text;

    }
}