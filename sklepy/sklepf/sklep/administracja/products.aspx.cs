﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class administracja_products : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["uzytkowniknumber"] == null&& Session["uzytkowniknazwa"]!=null&& Session["uzytkowniknazwa"].ToString()=="admin")
        {
            Response.Redirect("~/");
        }
    }
    protected void insertproduct(object sender, EventArgs e)
    {
        try
        {
            ProductsSqlDataSource.Insert();
        }
        catch (Exception exe) { }

    }
    protected void insertproductsql(object sender, SqlDataSourceCommandEventArgs e)
    {
        e.Command.Parameters["name"].Value = ((TextBox)GridView1.FooterRow.FindControl("tbname")).Text;
        e.Command.Parameters["quantity"].Value = ((TextBox)GridView1.FooterRow.FindControl("tbquantity")).Text;
        e.Command.Parameters["price"].Value = ((TextBox)GridView1.FooterRow.FindControl("tbprice")).Text;
        e.Command.Parameters["img"].Value = ((TextBox)GridView1.FooterRow.FindControl("tbimg")).Text;
        e.Command.Parameters["descripction"].Value = ((TextBox)GridView1.FooterRow.FindControl("tbdescripction")).Text;
        e.Command.Parameters["producent"].Value = ((TextBox)GridView1.FooterRow.FindControl("tbproducent")).Text;
    }
}