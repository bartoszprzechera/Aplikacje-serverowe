﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SiteMaster : MasterPage
{

    protected void Page_Init(object sender, EventArgs e)
    {
       
    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["uzytkowniknumber"] == null)
        {
           
                logged.Visible = false;
                anonymus.Visible = true;
                admin.Visible = false;
            

           
        }
        else {
     
            if (Session["uzytkowniknazwa"].ToString() == "admin")
            {
                logged.Visible = false;
                anonymus.Visible = false;
                admin.Visible = true;
            }
            else {
            logged.Visible = true;
            anonymus.Visible = false;
            admin.Visible = false;
            }
        }
        
    }

    protected void Logout(object sender, EventArgs e)
    {
        Session["uzytkowniknumber"] = null;
        Session["uzytkowniknazwa"] = null;
        Response.Redirect("~/");
    }
}