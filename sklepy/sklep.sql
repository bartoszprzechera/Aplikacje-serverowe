-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 15 Lis 2018, 22:29
-- Wersja serwera: 10.1.31-MariaDB
-- Wersja PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `sklep`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'default');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `products` varchar(1024) NOT NULL,
  `status` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `orders`
--

INSERT INTO `orders` (`id`, `user`, `products`, `status`) VALUES
(1, 1, '-1', 'new'),
(2, 1, '-1--1--2--3', 'new'),
(3, 1, '-1--1--2--3', 'new'),
(4, 1, '-5--6--2', 'new'),
(6, 2, '-1--5--2--3', 'zaplacono'),
(7, 3, '-1', 'nwe');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `img` mediumtext,
  `name` varchar(256) DEFAULT NULL,
  `descripction` varchar(1000) NOT NULL DEFAULT '',
  `producent` varchar(256) DEFAULT '',
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `products`
--

INSERT INTO `products` (`id`, `quantity`, `price`, `img`, `name`, `descripction`, `producent`, `category`) VALUES
(2, 1, 123, 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png', 'asd', 'asd', 'asd', 0),
(3, 123, 123, 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png', 'asd', 'asd', 'asd', 1),
(4, 123, 123, 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png', 'asdhjgghj', 'asd', 'asd', 0),
(5, 123, 123, 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png', 'asd', 'asd', 'asd', 1),
(6, 123, 123, 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png', 'asd', 'asdihjgi', 'asd', 0),
(7, 123, 123, 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png', 'asd', 'asd', 'asd', 1),
(8, 123, 123, 'sdfsdfsdf', 'sdfsdf', 'dfsdf', 'sdfsdf', 0),
(9, 123, 345345, 'asdas', 'asda', 'dasdasd', 'asd', 0),
(10, 123123123, 234234, '234234', 'asdasd', '234234', '234234', 0),
(11, 1231, 1231, '1231', '1231ssss', '1231', '1231', NULL),
(13, 123, 123, '123', '123', '123', '123', NULL),
(14, 123, 123, '123', '123', '123', '123', NULL),
(15, 543, 543, '453', '534', '534', '534', NULL),
(16, 123, 1599, 'https://images.morele.net/full/757623_0_f.jpeg', 'i5-6500', 'Nadszed? czas na zupe?nie nowy standard wydajno?ci komputerów.\r\n\r\nPoznaj najnowsze procesory Intel® Core™ 6-tej generacji, wyprodukowane w procesie technologicznym 14?nm.\r\n\r\n \r\n\r\nOd niespotykanej wcze?niej szybko?ci uruchamiania, poprzez obs?ug? dotykow?, a? po funkcje rozpoznawania g?osu i twarzy – bardzo szybko zdasz sobie spraw?, jak wysoko postawili?my poprzeczk?. Niezwykle krótki czas reakcji pozwala na b?yskawiczne prze??czanie pomi?dzy aplikacjami i przegl?danie zasobów sieci, a dzi?ki obs?udze rozdzielczo?ci 4K Ultra HD poznasz zupe?nie nowy poziom wra?e? multimedialnych.', 'Intel', 1),
(17, 123, 1599, 'https://images.morele.net/full/757623_0_f.jpeg', 'i5-6500', 'Nadszed? czas na zupe?nie nowy standard wydajno?ci komputerów.\r\n\r\nPoznaj najnowsze procesory Intel® Core™ 6-tej generacji, wyprodukowane w procesie technologicznym 14?nm.Od niespotykanej wcze?niej szybko?ci uruchamiania, poprzez obs?ug? dotykow?, a? po funkcje rozpoznawania g?osu i twarzy – bardzo szybko zdasz sobie spraw?, jak wysoko postawili?my poprzeczk?. Niezwykle krótki czas reakcji pozwala na b?yskawiczne prze??czanie pomi?dzy aplikacjami i przegl?danie zasobów sieci, a dzi?ki obs?udze rozdzielczo?ci 4K Ultra HD poznasz zupe?nie nowy poziom wra?e? multimedialnych.', 'Intel', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `mark` tinyint(4) NOT NULL DEFAULT '1',
  `user` varchar(256) NOT NULL,
  `comment` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `ratings`
--

INSERT INTO `ratings` (`id`, `product`, `mark`, `user`, `comment`) VALUES
(1, 2, 5, 'asdasd', 'asdasdsadas'),
(2, 2, 5, 'david ', 'igure hauaigre hhogreaai huoetri ohugr ighurowai guohyrewoia yuewfo iufhww afieohuwf ueoihawfe uoyighwagr iohua grweiuoag ruiyewhgar uhyiweagr uhyiwegr awhiuya iyurghwegr aweuyigr aewuiy'),
(3, 2, 22, 'anonymus', 'asdasdasdasdasdsad'),
(4, 2, 9, 'anonymus', 'chuj'),
(5, 2, 5, 'anonymus', 'chuj'),
(6, 2, 5, 'anonymus', 'chujsdfsdf'),
(7, 2, 0, 'anonymus', 'chujsdfsdf');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `activated` tinyint(1) DEFAULT '0',
  `city` varchar(256) DEFAULT NULL,
  `street` varchar(256) DEFAULT NULL,
  `postcode` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `activated`, `city`, `street`, `postcode`) VALUES
(3, 'easdasdasdasdasdachera@gmsdsdail.com', 'BartoszasdasdPrzasdasdechera@gmail.com', 'asdasasdasddsdsd', 0, 'Krakówasdasdsdsd', 'ul.Powstsdsdanców 26asdasd/112', '31-422'),
(4, 'admin', 'BartoszPrzechera@gmail.com', 'admin', 0, 'Kraków', 'ul.Powstanców 26/112', '31-422'),
(5, 'xscxfcvx', 'sd', 'cvxcv', NULL, 'xcvxcv', 'xcvxc', 'vxcv'),
(6, 'dfzbzdbf', 'agfdasgd', 'asgfd', NULL, 'asgd', 'asgdf', 'adsg'),
(7, 'dfzbzdb333333f', 'agfdas333gd', 'asgfd333', NULL, 'asgd333', 'asgdf333', 'adsg33333'),
(9, 'asdasdasd', 'BartoszPrzechera@gmail.com', 'asdasd', 0, 'asdasd', 'asdasd', 'asdasd'),
(10, '123123123', '123123123@wp.pl', '123123', 0, '123123', '123123', '123123'),
(11, '123123123', 'BartoszPrzechera@gmail.com', '123123', 0, 'Kraków', 'ul.Powstanców 26/112', '31-422'),
(12, '321', '321', '321', 0, '321', '312213', '321'),
(16, 'qweqwe', 'qweqwe@wp.pl', 'qweqwe', 0, '', '', ''),
(17, 'ASDASDASD', 'asdasdasd@asd.pl', 'ASDASDASD', 0, '', '', ''),
(18, 'chuj', 'chuj', 'chuj', 0, '', '', '');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indeksy dla tabeli `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT dla tabeli `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
