﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="View.aspx.cs" Inherits="GuestBook.View" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Xml ID="Xml1" runat="server" DocumentSource="~/book.xml" TransformSource="~/book.xslt"></asp:Xml>
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/GuestForm.aspx">Return to main site</asp:HyperLink>
        </div>
    </form>
</body>
</html>
