﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;

namespace ServerTCP
{
    class Client
    {
        public BinaryReader clients_r;
        public BinaryWriter clients_w;
        public TcpClient clients_c;
        public Thread clients_t;
        public Client(BinaryReader c_r, BinaryWriter c_w, TcpClient c_c, Thread c_t)
        {
            clients_r = c_r;
            clients_w = c_w;
            clients_c = c_c;
            clients_t = c_t;
        }
    }
    public partial class Form1 : Form
    {
        private TcpListener server;
       
        
        bool running = false;
        private List<Client> clients=new List<Client>();
        public Form1()
        {
            InitializeComponent();
        }
       

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bStrart_Click(object sender, EventArgs e)
        {
            bwConnection.RunWorkerAsync();
            bStrart.Enabled = false;
        }

        


        //lbHosts.Invoke(new MethodInvoker(delegate { lbHosts.Items.Add("Start a scan ..."); }));

        private void bwConnection_DoWork(object sender, DoWorkEventArgs e)
        {
            IPAddress addressIp = null;
            try
            {
                addressIp = IPAddress.Parse(tbAddress.Text);
            }
            catch
            {
                MessageBox.Show("Błędny adres");
                tbAddress.Text = String.Empty;
                return;
            }

            int port = System.Convert.ToInt16(nupPort.Value);
            server = new TcpListener(addressIp, port);
            try
            {
                server.Start();
                running = true;
                while (running) {
                    TcpClient client = server.AcceptTcpClient();
                    NetworkStream ns = client.GetStream();
                    BinaryReader reading = new BinaryReader(ns);
                    BinaryWriter writing = new BinaryWriter(ns);
                    lvLog.Invoke(new MethodInvoker(delegate { lvLog.Items.Add("New user connected."); }));
                    lvLog.Invoke(new MethodInvoker(delegate { lvLog.EnsureVisible(lvLog.Items.Count - 1); }));
                    if (reading.ReadString() == "pass")
                    {
                        Thread newclient = new Thread(() => Client(clients.Count));
                        clients.Add(new Client(reading, writing, client, newclient));
                        newclient.Start();
                        lvLog.Invoke(new MethodInvoker(delegate { lvLog.Items.Add("User autorizaction comleate."); }));
                        lvLog.Invoke(new MethodInvoker(delegate { lvLog.EnsureVisible(lvLog.Items.Count - 1); }));
                        //MessageBox.Show("pass correct");
                        //bwClient.RunWorkerAsync();
                    }
                    else {
                        lvLog.Invoke(new MethodInvoker(delegate { lvLog.Items.Add("User autorizaction fail."); }));
                        lvLog.Invoke(new MethodInvoker(delegate { lvLog.EnsureVisible(lvLog.Items.Count - 1); }));
                        //MessageBox.Show("pass incorrect");
                    }
                }
                server.Stop();


            }
            catch (Exception ex)
            {
                lvLog.Invoke(new MethodInvoker(delegate { lvLog.Items.Add("Błąd inicjacji servera!"); }));
                lvLog.Invoke(new MethodInvoker(delegate { lvLog.EnsureVisible(lvLog.Items.Count - 1); }));
                MessageBox.Show(ex.ToString(), "Błąd");
            }
        }

       
      

        private void bsend_Click(object sender, EventArgs e)
        {
            //writing.Write(tb_message.Text);
            //lvLog.Items.Add(tb_message.Text);
        }
        void Client(int num) {
            num--;
            string message_recived;
            try
            {


                while ((message_recived = clients[num].clients_r.ReadString()) != "END")
                {
                    for (int i = 0; i < clients.Count; i++)
                    {
                        try
                        {
                            clients[i].clients_w.Write(message_recived);
                        }
                        catch { }
                    }
                    lvLog.Invoke(new MethodInvoker(delegate { lvLog.Items.Add(message_recived); }));
                    lvLog.Invoke(new MethodInvoker(delegate { lvLog.EnsureVisible(lvLog.Items.Count - 1); }));
                }
            }
            catch {
                try
                {
                    clients[num]=null;
                }
                catch {
                }

            }
           
        }
    }
}
