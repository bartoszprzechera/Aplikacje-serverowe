#include <iostream>
#include <vector>
#include <locale>
#include <fstream>
#include <string>
#include <iomanip>
#include <cstdlib>
#include <time.h>
#include <math.h>

using namespace std;
int pot_mod(int a, int w, int n)
{
  int pot,wyn,q;

// wyk�adnik w rozbieramy na sum� pot�g 2
// przy pomocy algorytmu Hornera. Dla reszt
// niezerowych tworzymy iloczyn pot�g a modulo n.

  pot = a; wyn = 1;
  for(q = w; q > 0; q /= 2)
  {
    if(q % 2) wyn = (wyn * pot) % n;
    pot = (pot * pot) % n; // kolejna pot�ga
  }
  return wyn;
}
int odwr_mod(int a, int n)
{
  int a0,n0,p0,p1,q,r,t;

  p0 = 0; p1 = 1; a0 = a; n0 = n;
  q  = n0 / a0;
  r  = n0 % a0;
  while(r > 0)
  {
    t = p0 - q * p1;
    if(t >= 0)
      t = t % n;
    else
      t = n - ((-t) % n);
    p0 = p1; p1 = t;
    n0 = a0; a0 = r;
    q  = n0 / a0;
    r  = n0 % a0;
  }
  return p1;
}
int nwd(int a, int b)
{
  int t;

  while(b != 0)
  {
    t = b;
    b = a % b;
    a = t;
  };
  return a;
}
int klucz[3];
void gen_rsa_keys(){
	const int tp[10] = {11,13,17,19,23,29,31,37,41,43};
	int p,q,o,n,e,d;
	do
  {
    p = tp[rand() % 10];
    q = tp[rand() % 10];
  } while (p == q);
  o=(p-1)*(q-1);
  n=p*q;
  
  for(e = 3; nwd(e,o) != 1; e += 2);
  d = odwr_mod(e,o);
  klucz[0]=e;
  klucz[1]=n;
  klucz[2]=d;
  
}
int szyfrowanie(int t,int e,int n){
	return pot_mod(t,e,n);
}
int odszyfrowanie(int t,int d,int n){
	return pot_mod(t,d,n);
}
int main()
{
	srand((unsigned)time(NULL));
	gen_rsa_keys();

	cout<<szyfrowanie(5,klucz[0],klucz[1])<<endl;
	cout<<odszyfrowanie(szyfrowanie(5,klucz[0],klucz[1]),klucz[2],klucz[1])<<endl;
}
